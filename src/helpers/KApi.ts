import axios, { AxiosInstance } from 'axios';

export default class KApi {
    private apiKey: string;
    private baseUrl: string;
    private axiosInstance: AxiosInstance;
    constructor() {
        this.apiKey = process.env['API_KEY'] || 'NO_API_KEY';
        this.baseUrl = 'https://kesko.azure-api.net/v1';
        this.axiosInstance = axios.create({
            baseURL: this.baseUrl,
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json',
                'Ocp-Apim-Subscription-Key': this.apiKey,
            },
        });
    }

    post(endpoint: string, body: object) {
        return this.axiosInstance.post(endpoint, JSON.stringify(body));
    }
}