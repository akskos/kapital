import Router from 'koa-router';
import { ProductController } from './controllers/ProductController';
import { Controller } from './controllers/Controller';

export default class AppRouter {
    private koaRouter: Router;
    constructor(private controller: Controller) {
        this.koaRouter = new Router();
        this.registerRoutes();
    }

    routes() {
        return this.koaRouter.routes();
    }

    private registerRoutes() {
        this.koaRouter.get('/products', this.controller.getProducts);
        this.koaRouter.get('/recipes', this.controller.getRecipes);
        this.koaRouter.get('/recipe/:id/ingredients', this.controller.getIngredientsForRecipe);
        this.koaRouter.get('/products-on-sale', this.controller.getProductsOnSale);
        this.koaRouter.get('/recipes-on-sale', this.controller.getRecipesOnSale);
        this.koaRouter.get('/themed-bundles', this.controller.getRandomBundles);
    }
};