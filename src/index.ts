import container from './dic';
import * as Koa from 'koa';

container.resolve<Koa>('app').listen(process.env.PORT);