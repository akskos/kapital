import * as awilix from 'awilix';
import Koa from 'koa';
import { Controller } from './controllers/Controller';

import AppRouter from './AppRouter';
import KApi from './helpers/KApi';

const container = awilix.createContainer({
    injectionMode: awilix.InjectionMode.CLASSIC,
});

function makeApp() {
    const app = new Koa();
    app.use(container.resolve<AppRouter>('router').routes());
    return app;
}

container.register('app', awilix.asFunction(makeApp));
container.register('router', awilix.asClass(AppRouter));
container.register('controller', awilix.asClass(Controller));
container.register<KApi>('kapi', awilix.asClass(KApi));

export default container;