import { Context } from "koa";
import KApi from "../helpers/KApi";

export class Controller {
    constructor(private kapi: KApi) { }

    getProducts = async (ctx: Context) => {
        const result = await this.kapi.post('/search/products', {
            filters: {
            },
        });
        result.data.numberOfProducts = result.data.results.length;
        ctx.response.body = result.data;
    }

    getRecipes = async (ctx: Context) => {
        const diet = ctx.query.diet;
        const ingredientType = ctx.query.withProduct;
        let searchOpt: { filters: any[] } = {
            filters: [],
        };
        if (ingredientType) {
            searchOpt.filters.push({
                name: 'ingredientType',
                value: ingredientType,
            });
        }
        const result = await this.kapi.post('/search/recipes', searchOpt);
        const recipes = result.data.results.filter((recipe: any) => {
            if (!diet) {
                return true;
            } else if (recipe.SpecialDiets && recipe.SpecialDiets.find((specialDiet: any) => specialDiet.Name === diet)) {
                return true;
            } else {
                return false;
            }
        });
        result.data.results = recipes;
        ctx.response.body = result.data;
    }

    getIngredientsForRecipe = async (ctx: Context) => {
        const recipeId = ctx.params.id;
        const result = await this.kapi.post('/search/recipes', {
            filters: {
                ids: recipeId,
            },
        });
        const recipes = result.data.results;
        let ingredients: any[] = [];
        recipes[0].Ingredients.forEach((category: any) => {
            ingredients = ingredients.concat(category.SubSectionIngredients);
        });
        const ingredientTypes = ingredients.map((ingredient: any) => {
            return ingredient[0].IngredientType;
        }).filter(type => type !== undefined);
        console.log(ingredientTypes);
        const products = await this.kapi.post('/search/products', {
            filters: [
                {
                    name: 'ingredientType',
                    value: ingredientTypes,
                    operator: 'or',
                }
            ],
        });
        products.data.results.forEach((r: any) => {
            r.price = Math.ceil(Math.random() * 8);
            r.discountPercentage = Math.ceil(Math.random() * 50) / 100;
        });
        ctx.response.body = products.data;
    }

    getRecipesOnSale = async (ctx: Context) => {
        const productsOnSale = await this.fetchProductsOnSale();
        const ingredientTypeIDs = productsOnSale.results.map((product: any) => {
            return product.ingredientType && product.ingredientType.id;
        }).filter((id: any) => id !== undefined);
        console.log(ingredientTypeIDs);
        const result = await this.kapi.post('/search/recipes', {
            filters: [
                {
                    name: 'ingredientType',
                    value: ingredientTypeIDs,
                    operator: 'or',
                }
            ],
        });
        let recipes = result.data.results;
        recipes = recipes.slice(0, 6);
        result.data.results = recipes;
        ctx.response.body = result.data;
    }

    fetchProductsOnSale = async () => {
        let offset = 11000;
        const result = await this.kapi.post('/search/products', {
            filters: {
            },
            view: {
                offset,
            },
        });
        return result.data;
    }

    async makeProductBundle() {
        let offset = 11000;
        const result = await this.kapi.post('/search/products', {
            filters: {
            },
            view: {
                offset,
            },
        });
        result.data.results.forEach((r: any) => {
            r.price = Math.ceil(Math.random() * 8);
            r.discountPercentage = Math.ceil(Math.random() * 50) / 100;
        });
        const begin = Math.floor(Math.random() * 50) + 1;
        const products = result.data.results.slice(begin, begin + 4 + Math.ceil(Math.random() * 10));
        result.data.results = products;
        return result.data;
    }

    getRandomBundles = async (ctx: Context) => {
        let bundles = [];
        for (let i = 0; i < 6; i++) {
            bundles.push(await this.makeProductBundle());
        }
        bundles[0].name = 'Vegan bundle';
        bundles[1].name = 'Junction bundle';
        bundles[2].name = 'Black out Friday bundle';
        bundles[3].name = 'Healthy bundle';
        bundles[4].name = 'Protein bundle';
        bundles[5].name = 'Christmas bundle';
        ctx.response.body = bundles;
    }

    getProductsOnSale = async (ctx: Context) => {
        let response = await this.fetchProductsOnSale();
        response.results.forEach((r: any) => {
            r.price = Math.ceil(Math.random() * 8);
            r.discountPercentage = Math.ceil(Math.random() * 50) / 100;
        });
        ctx.response.body = response;
    }
}