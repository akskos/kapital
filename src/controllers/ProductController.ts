import { Context } from "koa";
import KApi from "../helpers/KApi";

export class ProductController {
    constructor(private kapi: KApi) { }

    getProducts = async (ctx: Context) => {
        const result = await this.kapi.post('/search/products', {
            filters: {
                brand: 'pirkka',
            },
        });
        ctx.response.body = result.data;
    }
}